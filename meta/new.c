//
//  id.cpp
//  ooc
//
//  Created by Yuri Bukhalenkov on 12/21/17.
//  Copyright (c) 2017 home. All rights reserved.
//

#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include "new.h"


//================== memory managment functions
id new( const struct CMetaClass* type, ... )
{
    if ( NULL == type )
        return NULL;

    void* p = calloc( 1, type->size + sizeof(struct CMetaClass*) );
    
    //  1. set metaclass
    const struct CMetaClass** class = p;
    *class = type;
    p     += sizeof(struct CMetaClass*);
    
    //  2. constructor call
    if ( type->ctor )
    {
        va_list ap;
        
        va_start( ap, type );
        
        p = type->ctor( p, &ap );
        
        va_end( ap );
    }

    printf( "%p created\n", p );
    
    return p;
}

void delete( id object )
{
    printf( "destroying %p\n", object );

    const struct CMetaClass* class = getMetaClass( object );
    
    if ( class )
    {
        class->dtor && (class->dtor(object), true);

        free( object - sizeof(struct CMetaClass*) );
    }
}
