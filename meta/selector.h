//
//  selector.h
//  ooc
//
//  Created by Yuri Bukhalenkov on 12/21/17.
//  Copyright (c) 2017 home. All rights reserved.
//

#ifndef OOC_SELECTOR_HADER
#define OOC_SELECTOR_HADER

#include "id.h"


typedef void (*selector)(id object);


//================== calls functions
void call    ( id object, const char* method );
void add_call( struct CMetaClass* class, const char* method, selector sel );


#endif /* OOC_SELECTOR_HADER */