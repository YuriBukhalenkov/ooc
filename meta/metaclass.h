//
//  metaclass.h
//  ooc
//
//  Created by Yuri Bukhalenkov on 7/25/12.
//  Copyright (c) 2012 home. All rights reserved.
//

#ifndef OOC_METACLASS_HEADER
#define OOC_METACLASS_HEADER

#include <stdbool.h> 
#include <stdarg.h>
#include "id.h"


//================== class CMetaClass
struct CMetaClass
{
    size_t size;
    
    const char* name;
    
    id   (*ctor) (id self, va_list* app); 
    void (*dtor) (id self);
    
    void (*draw) (const id self);
    
    struct SCall* calls;
};


//================= CMetaClass functions
static inline const struct CMetaClass* getMetaClass( const id object )
{
    return object ? *((const struct CMetaClass**)object - 1) : 0; 
}

static inline bool isKindOf( const id object, const struct CMetaClass* class )
{
    return getMetaClass(object) == class;
}

const struct CMetaClass* class( const char* name );

static inline void draw( const id object )
{
    const struct CMetaClass* class = getMetaClass( object );
    
    if ( class && class->draw )
    {
        class->draw( object );
    }
}


//================== metaclasses registration
void Register( const struct CMetaClass* class );


#endif // OOC_METACLASS_HEADER
