//
//  new.h
//  ooc
//
//  Created by Yuri Bukhalenkov on 12/21/17.
//  Copyright (c) 2017 home. All rights reserved.
//

#ifndef OOC_NEW_HEADER
#define OOC_NEW_HEADER

#include "metaclass.h"


//================== memory managment functions
id   new   ( const struct CMetaClass* type, ... );
void delete( id object );

#define AUTO_DTOR_CALL __attribute__((cleanup (auto_destroy)))

static inline void auto_destroy( id* pObject )
{
    if ( pObject )
    {
        delete( *pObject );
    }
}


#endif /* OOC_NEW_HEADER */