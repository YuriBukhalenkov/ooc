//
//  selector.c
//  ooc
//
//  Created by Yuri Bukhalenkov on 12/21/17.
//  Copyright (c) 2017 home. All rights reserved.
//

#include <string.h>
#include <stdlib.h>
#include "metaclass.h"
#include "selector.h"


//================= struct SCall
struct SCall
{
    const struct SCall* next;
    const char*         method_name;
    selector            sel;
};


//================== calls functions
static inline struct SCall* InsertSelector( const struct SCall* tail, const char* metod, selector sel )
{
    struct SCall* item;
    
    item = calloc( 1, sizeof(struct SCall) );
    item->next        = tail;
    item->method_name = strdup(metod);
    item->sel         = sel;
    
    return item;
}

void call( id object, const char* method )
{
    const struct CMetaClass *metaclass = getMetaClass( object );
    const struct SCall      *item      = metaclass ? metaclass->calls : NULL;
    
    selector sel = NULL;
    
    for ( ; item; item = item->next ) 
    {
        if ( 0 == strcmp(item->method_name, method) )
            sel = item->sel;
    }
    
    if ( sel )
    {
        (*sel)( object );
    }
}

void add_call( struct CMetaClass* class, const char* method, selector sel )
{
    if ( class && method && sel )
    {
        class->calls = InsertSelector( class->calls, method, sel );
    }
}