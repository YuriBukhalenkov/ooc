//
//  metaclass.c
//  ooc
//
//  Created by Yuri Bukhalenkov on 7/26/12.
//  Copyright (c) 2012 home. All rights reserved.
//

#include <string.h>
#include <stdlib.h>
#include "metaclass.h"


static const struct SNode* root = NULL;


//================= struct SNode
struct SNode
{
    const struct SNode*      next;
    const struct CMetaClass* class;
};


//================== metaclasses registration
static inline struct SNode* InsertClass( const struct SNode* tail, const struct CMetaClass* class )
{
    struct SNode* item;
    
    item = calloc( 1, sizeof(struct SNode) );
    item->next  = tail;
    item->class = class;
    
    return item;
}

void Register( const struct CMetaClass* class )
{
    root = InsertClass( root, class );
}

const struct CMetaClass* class( const char* name )
{
    const struct SNode* item = root;
    
    while ( item ) 
    {
        if ( 0 == strcmp(item->class->name, name) )
            return item->class;
        
        item = item->next;
    }
    
    return NULL;
}
