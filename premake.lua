solution "ooc"
  configurations { "Debug", "Release" }

  configuration { "Debug" }
     targetdir "build/Debug"

  configuration { "Release" }
    targetdir "build/Release"

  configuration { "linux", "gmake" }
    buildoptions { "-Wreturn-type" }


-- ooc project
project "ooc"
  language "C"
  kind     "ConsoleApp"

  includedirs { "./", "meta" }

  files  { "*.h", "*.c" }
  files  { "shapes/*h", "shapes/*.c" }
  files  { "meta/*h", "meta/*.c" }

  configuration { "Debug" }
    defines { "_DEBUG", "DEBUG" }
    flags   { "Symbols" }
    objdir  ( "build/" .. project().name .. "/Debug" )

  configuration { "Release" }
    defines { "NDEBUG" }
    flags   { "Optimize" }
    objdir  ( "build/" .. project().name .. "/Release" )