//
//  circle.h
//  ooc
//
//  Created by Yuri Bukhalenkov on 7/25/12.
//  Copyright (c) 2012 home. All rights reserved.
//

#ifndef OOC_CIRCLE_HEADER
#define OOC_CIRCLE_HEADER

#include "metaclass.h"
#include "point.h"


//================== metaclass
extern struct CMetaClass metaCircle;


//================== class CCircle
struct CCircle
{
    struct CPoint super;  // inheritance
    
    int rad;
};


//================= public methods
id   Circle_ctor( id self, va_list* ap );
void Circle_dtor( id self );
void Circle_draw( const id self );


#endif // OOC_CIRCLE_HEADER
