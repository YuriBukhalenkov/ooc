//
//  rect.h
//  ooc
//
//  Created by Yuri Bukhalenkov on 12/20/17.
//  Copyright (c) 2017 home. All rights reserved.
//

#ifndef OOC_RECT_HEADER
#define OOC_RECT_HEADER

#include "metaclass.h"
#include "point.h"


//================= struct SSize
struct SSize
{
	unsigned int height;
	unsigned int width;
};


//================== metaclass
extern struct CMetaClass metaRect;


//================== class CRect
struct CRect
{    
    struct CPoint origin;
    struct SSize  size;
};


//================= public methods
id   Rect_ctor( id self, va_list* ap );
void Rect_dtor( id self );
void Rect_draw( const id self );

#endif /* OOC_RECT_HEADER */