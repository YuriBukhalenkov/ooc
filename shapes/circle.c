//
//  circle.c
//  ooc
//
//  Created by Yuri Bukhalenkov on 7/25/12.
//  Copyright (c) 2012 home. All rights reserved.
//

#include <stdio.h>
#include "circle.h"
#include "selector.h"


#define PI 3.14159


//================== selectors
static void area( id object )
{
    const struct CCircle* obj = object;

    printf( "circle area: %f\n", 2 * PI * obj->rad * obj->rad );
}


//================== metaclass
struct CMetaClass metaCircle = { sizeof(struct CCircle), "circle", Circle_ctor, Circle_dtor, Circle_draw };

static void CircleMetaclassRegister() __attribute__((constructor));

void CircleMetaclassRegister()
{
    Register( &metaCircle );

    add_call( &metaCircle, "area", area );
}


//================== CCircle Implementation
id Circle_ctor( id _self, va_list* ap )
{
    puts( "Circle constructor" );
    
    struct CCircle* self = _self;

    // ctor for super class
    Point_ctor( &self->super, ap );
    
    self->rad = va_arg( *ap, int );
    
    return self;
}

void Circle_dtor( id _self )
{    
    puts( "Circle destructor" );
}

void Circle_draw( const id _self )
{
    const struct CCircle* self = _self;
    
    if ( self )
    {
        printf( "Circle drawing: x = %d, y = %d, r = %d\n", self->super.x, self->super.y, self->rad );
    }
}