//
//  rect.c
//  ooc
//
//  Created by Yuri Bukhalenkov on 12/20/17.
//  Copyright (c) 2017 home. All rights reserved.
//

#include <stdio.h>
#include <stdarg.h>
#include "rect.h"
#include "selector.h"


//================== selectors
static void area( id object )
{
    const struct CRect* obj = object;

    printf( "rect area: %u\n", obj->size.height * obj->size.width );
}


//================== metaclass
struct CMetaClass metaRect = { sizeof(struct CRect), "rect", Rect_ctor, Rect_dtor, Rect_draw };

static void RectMetaclassRegister() __attribute__((constructor));

void RectMetaclassRegister()
{
    Register( &metaRect );

    add_call( &metaRect, "area", area );
}


//================== CRect Implementation
id Rect_ctor( id _self, va_list* ap )
{
    puts( "Rect constructor" );
    
    struct CRect* self = _self;

    // ctor for member class
    Point_ctor( &self->origin, ap );
    
    self->size.width  = va_arg( *ap, int );
    self->size.height = va_arg( *ap, int );
    
    return self;
}

void Rect_dtor( id _self )
{    
    puts( "Rect destructor" );
}

void Rect_draw( const id _self )
{
    const struct CRect* self = _self;
    
    if ( self )
    {
        printf( "Rect drawing: x1 = %d, y1 = %d, y2 = %d, y2 = %d\n", 
        	self->origin.x, self->origin.y, 
        	self->origin.x + self->size.width,
        	self->origin.y + self->size.height );
    }
}