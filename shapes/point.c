//
//  point.c
//  ooc
//
//  Created by Yuri Bukhalenkov on 7/25/12.
//  Copyright (c) 2012 home. All rights reserved.
//

#include <stdio.h>
#include <stdarg.h>
#include "point.h"


//================== metaclass
struct CMetaClass metaPoint = { sizeof(struct CPoint), "point", Point_ctor, 0, Point_draw };

static void PointMetaclassRegister() __attribute__((constructor));

void PointMetaclassRegister()
{
    Register( &metaPoint );
}


//================== CPoint Implementation
id Point_ctor( id _self, va_list* ap )
{
    puts( "Point constructor" );
    
    struct CPoint* self = _self;
    
    self->x = va_arg( *ap, int );
    self->y = va_arg( *ap, int );
    
    return self;
}

void Point_draw( const id _self )
{
    const struct CPoint* self = _self;
    
    if ( self )
    {
        printf( "Point drawing: x = %d, y = %d\n", self->x, self->y );
    }
}
