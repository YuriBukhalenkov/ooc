//
//  point.h
//  ooc
//
//  Created by Yuri Bukhalenkov on 7/25/12.
//  Copyright (c) 2012 home. All rights reserved.
//

#ifndef OOC_POINT_HEADER
#define OOC_POINT_HEADER

#include "metaclass.h"


//================== metaclass
extern struct CMetaClass metaPoint;


//================== class CPoint
struct CPoint
{    
    int x, y;
};


//================= public methods
id   Point_ctor( id self, va_list* ap );
void Point_draw( const id self );


#endif // OOC_POINT_HEADER
