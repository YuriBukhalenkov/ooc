//
//  main.c
//  ooc
//
//  Created by Yuri Bukhalenkov on 7/24/12.
//  Copyright (c) 2012 home. All rights reserved.
//

#include <stdio.h>
#include "new.h"
#include "selector.h"
#include "shapes/point.h"
#include "shapes/circle.h"


//================== selector
static void info( id object )
{
    printf( "object (%p) is%spoint\n",
        object, isKindOf(object, &metaPoint) ? " " : " not " );
    
    printf( "object (%p) is%scircle\n", 
        object, isKindOf(object, &metaCircle) ? " " : " not " );
    
    printf( "object\'s (%p) class name is \"%s\"\n", 
        object, getMetaClass(object)->name );
}

static inline void area( id object )
{
    call( object, "area" );
}


//================== main function
int main ( int argc, const char* argv[] )
{    
    id obj1 = new( class("point"), 1, 2 );    // class w/out dtor, virtual ctor
    id obj2 = new( class("circle"), 3, 4, 5 );// virtual ctor
    id obj3 AUTO_DTOR_CALL;                   // auto delete
    obj3    = new( &metaCircle, 6, 7 );       // without 3rd param (radius)
    id obj4 = new( class("ghost") );          // non existing class
    id obj5 = new( class("rect"), 1, 2, 10, 20 );
        
    // call virtual methods
    draw( obj1 );
    draw( obj2 );
    draw( obj3 );
    draw( obj4 );
    draw( obj5 );
    
    // adding methods to class
    add_call( &metaPoint, "info", info );
    // adding method to non existing class
    add_call( (struct CMetaClass*)class("ghost"), "info", info );
    
    call( obj1, "info" );
    call( obj2, "info" ); // nothing should happend
    call( obj4, "info" ); // call method for non existing object

    area( obj2 );
    area( obj5 );
    
    // delete objects with calling virtual destructor
    delete( obj1 );
    delete( obj2 );
    // delete( obj3 ); this object should be destroyed automatically
    delete( obj4 );
    delete( obj5 );
    
    return 0;
}
